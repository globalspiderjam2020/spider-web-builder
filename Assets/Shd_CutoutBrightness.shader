﻿Shader "Custom/Shd_CutoutBrightness"
 {
     Properties
     {
        _XSpeed ("Speed X", float) = 0
        _YSpeed ("Speed Y", float) = 0
         _Color ("Main Color", Color) = (1,1,1,1)
         _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
         _AlphaTex ("Alpha", 2D) = "white" {}
        _BumpMap ("Bumpmap", 2D) = "bump" {}
         _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        _Brightness ("Brightness", float) = 0
     }
 
     SubShader
     {
         Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
         LOD 300
         Cull off
         
         CGPROGRAM
         #pragma surface surf Lambert alpha vertex:vert
 
         sampler2D _MainTex;
         sampler2D _AlphaTex;
      sampler2D _BumpMap;
         fixed4 _Color;
         fixed _Cutoff;
        fixed _XSpeed;
        fixed _YSpeed;
        fixed _Brightness;
 
         struct Input
         {
             float2 uv_MainTex;
             float2 uv_AlphaTex;
            float2 uv_BumpMap;
         };

        void vert (inout appdata_full v) {
        //    float3 p = v.vertex.xyz;
        //    p.y = sin(p.x * _Timy);
        //    v.vertex.xyz = p;
        }
 
         void surf (Input IN, inout SurfaceOutput o)
         {
            fixed2 scrolledUV = IN.uv_MainTex;
            fixed xScrollValue = frac(sin(_XSpeed * _Time.y)/20);
            fixed yScrollValue = frac(_YSpeed * _Time.y);
            scrolledUV += fixed2(xScrollValue, yScrollValue);

             fixed4 MAIN = tex2D(_MainTex, scrolledUV) * _Color;
             o.Albedo = MAIN.rgb * _Brightness;
             if ((MAIN.r + MAIN.g + MAIN.b) / 3 < _Cutoff)
             {
                 MAIN.a = 0;
             }
             o.Alpha = MAIN.a;
            o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
         }
         ENDCG
     }
 
     FallBack "Transparent/Cutout/Diffuse"
 }
