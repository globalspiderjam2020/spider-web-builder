﻿Shader "Custom/Shd_ColorReplaceFive"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_ColorDark ("Dark Color", Color) = (0, 0, 0, 0)
		_ColorMediumDark ("Medium Dark Color", Color) = (0, 0, 0, 0)
		_ColorMedium ("Medium Color", Color) = (0, 0, 0, 0)
		_ColorMediumLight ("Medium Light Color", Color) = (0, 0, 0, 0)
		_ColorLight ("Light Color", Color) = (0, 0, 0, 0)
		_ThresholdDark ("Dark Threshold", float) = .33
		_ThresholdMediumDark ("Medium Dark Threshold", float) = .5
		_ThresholdMedium ("Medium Threshold", float) = .66
		_ThresholdMediumLight ("Medium Light Threshold", float) = .75
		_ThresholdLight ("Light Threshold", float) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 _ColorDark;
			fixed4 _ColorMediumDark;
			fixed4 _ColorMedium;
			fixed4 _ColorMediumLight;
			fixed4 _ColorLight;

			float _ThresholdDark;
			float _ThresholdMediumDark;
			float _ThresholdMedium;
			float _ThresholdMediumLight;
			float _ThresholdLight;


			fixed4 frag (v2f i) : SV_Target
			{

				fixed4 col = tex2D(_MainTex, i.uv);
				float alpha = col.a;
				float brightness = (col.r + col.g + col.b) / 3;

				if (brightness <= _ThresholdDark)
				{
					col = _ColorDark;
				}
				else if (brightness <= _ThresholdMediumDark)
				{
					col = _ColorMediumDark;
				}
				else if (brightness <= _ThresholdMedium)
				{
					col = _ColorMedium;
				}
				else if (brightness <= _ThresholdMediumLight)
				{
					col = _ColorMediumLight;
				}
				else
				{
					col = _ColorLight;
				}
				col.a = alpha;

				return col;
			}
			ENDCG
		}
	}
}
