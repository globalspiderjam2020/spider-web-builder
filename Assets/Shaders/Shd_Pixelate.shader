﻿Shader "Custom/Shd_Pixelate"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_XResolution ("Float", float) = 160
		_YResolution ("Float", float) = 90
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float _XResolution;
			float _YResolution;

			fixed4 frag (v2f i) : SV_Target
			{
				float2 uv = i.uv;
				uv.x *= _XResolution;
				uv.y *= _YResolution;
				uv.x = round(uv.x);
				uv.y = round(uv.y);
				uv.x /= _XResolution;
				uv.y /= _YResolution;

				fixed4 col = tex2D(_MainTex, uv);

				// fixed4 black = fixed4(0, 0, 0, 1);
				// fixed4 blackout = fixed4(1, 1, 1, 0);
				// if (col.r == 0)
				// {
				// 	col.a = .5f;
				// }
				//float r = 1;
				//float g = 1;
				//float b = 1;

				//if (col.r > col.g)
				//{
				//	g = .9f;
				//	if (col.r > col.b)
				//	{
				//		b = .9f;
				//	}
				//	else
				//	{
				//		r = .9f;
				//	}
				//}
				//else
				//{
				//	r = .9f;
				//	if (col.g > col.b)
				//	{
				//		b = .9f;
				//	}
				//	else
				//	{
				//		g = .9f;
				//	}
				//}


				//// just invert the colors
				//if ((col.r + col.g + col.b) / 3 < .05f)
				//{
				//	col = float4(r, g, b, col.a);
				//}
				//else if ((col.r + col.g + col.b) / 3 < .25f)
				//{
				//	col = float4(r / 2, g / 2, b / 2, col.a);
				//}
				//else
				//{
				//	col = float4(r / 5, g / 5, b / 5, col.a);
				//}
				col.a = 1;
				return col;
			}
			ENDCG
		}
	}
}
