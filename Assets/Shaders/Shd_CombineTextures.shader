﻿Shader "Custom/Shd_CombineTextures"
{
    Properties
    {
        _Texture1 ("Camera 1", 2D) = "white" {}
        _Texture2 ("Camera 2", 2D) = "white" {}
        _Depth1 ("Depth 1", 2D) = "white" {}
        _Depth2 ("Depth 2", 2D) = "white" {}
        _Texture3 ("Camera 3", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "RenderQueue"="Transparent"}
        Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _Texture1;
            sampler2D _Texture2;
            sampler2D _Depth1;
            sampler2D _Depth2;
            sampler2D _Texture3;

            v2f vert (appdata v)
            {
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col;
                fixed4 col_1 = tex2D(_Texture1, i.uv);
                fixed4 col_2 = tex2D(_Texture2, i.uv);
                fixed4 depth_1 = tex2D(_Depth1, i.uv);
                fixed4 depth_2 = tex2D(_Depth2, i.uv);
                fixed4 col_3 = tex2D(_Texture3, i.uv);

                col = col_2;

                if (depth_1.r < depth_2.r)
                {
                    if (depth_1.r != 1)
                    {
                        if (col.a < .1)
                        {
                            //col += col_1;
                        }
                        else
                        {
                            col = col_1;
                        }
                    }
                }

                // if (((col.r + col.g + col.b) / 3) > .5f)
                // {
                //     col = col_3;
                // }

                col.a = 1;
                return col;
            }
            ENDCG
        }
    }
}
