﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Unlit alpha-cutout shader.
// - no lighting
// - no lightmap support
// - no per-material color

Shader "Unlit/Shd_VertexAnim" {
Properties {
        _Speed ("Speed", float) = 0
        _Amplitude ("Amplitude", float) = 0
		_Wavelength("Amount", float) = 0
         _Color ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	_StaticMap ("Height Map", 2D) = "white" {}
	_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5

}
SubShader {
	Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
	LOD 500

	Lighting Off

	Pass {  
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				half2 texcoord : TEXCOORD0;
				UNITY_FOG_COORDS(1)
			};
        fixed _Speed;
        fixed _Amplitude;
        fixed _Wavelength;
        fixed4 _Color;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _StaticMap;
			float4 _StaticMap_ST;
			float _Cutoff;

			v2f vert (appdata_t v)
			{
				v2f o;
				float3 p = v.vertex.xyz;

				float k = 2 * UNITY_PI / _Wavelength;
				p.x += _Amplitude * sin(k * (p.x - _Speed * _Time.y));
				p.z += _Amplitude * cos(k * (p.z - _Speed * _Time.y));

				v.vertex.xyz = p;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				//UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
                //fixed xScrollValue = frac(sin(_XSpeed * _Time.y)/20);
                //fixed yScrollValue = frac(_YSpeed * _Time.y);
                //scrolledUV += fixed2(xScrollValue, yScrollValue);
				fixed4 col = tex2D(_MainTex, i.texcoord) * _Color;
				//UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
		ENDCG
	}
}

}