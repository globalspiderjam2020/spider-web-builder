﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderMunch : MonoBehaviour
{
    private GenericSoundPlayer mySoundPlayer;

    public GameObject eatParticles;
    // Start is called before the first frame update
    void Start()
    {
        mySoundPlayer = GetComponent<GenericSoundPlayer>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("fly"))
        {
            Instantiate(eatParticles, other.transform.position, Quaternion.identity);
            mySoundPlayer.EmitSound();
            GameManager.Instance.IncrementFlyCount();
            GameManager.Instance.cameraShakeScript.FlyEatCamShake();
            Destroy(other.gameObject);
        }
    }
}
