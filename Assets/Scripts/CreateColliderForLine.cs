﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateColliderForLine : MonoBehaviour
{
    // Start is called before the first frame update


    private void Awake()
    {
        GetComponent<Rigidbody>().isKinematic = true;
    }
    void Start()
    {
        //MakeBoxColliderFitLine();
        StartCoroutine (WaitBeforeMovingCollider());
    }

    IEnumerator WaitBeforeMovingCollider()
    {
        yield return new WaitForSeconds(0.1f);
        MakeBoxColliderFitLine();
    }

    void MakeBoxColliderFitLine()
    {
        LineRenderer myLineRenderer = GetComponent<LineRenderer>();

        //Vector3[] linePoints = myLineRenderer.GetPositions(out Vector3[] positions);

        float lineDistance = Vector3.Distance(myLineRenderer.GetPosition(0), myLineRenderer.GetPosition(1));

        transform.LookAt(myLineRenderer.GetPosition(1));

        

        BoxCollider myBox = GetComponent<BoxCollider>();
        // myBox.isTrigger = true;

        myBox.center = new Vector3(0f, 0f, lineDistance / 2f);

        myBox.size = new Vector3 (0.2f, 0.2f, lineDistance);

        myBox.enabled = true;

        GetComponent<Rigidbody>().isKinematic = false;
    }
}
