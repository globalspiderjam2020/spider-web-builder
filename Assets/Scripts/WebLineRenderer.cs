﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebLineRenderer : MonoBehaviour
{

    private LineRenderer swingLineRenderer;
    private ConfigurableJoint swingJoint;
    public Vector3 backupPosition;

    private bool isWebStillAttached = true;

    void Start()
    {
        swingLineRenderer = GetComponent<LineRenderer>();
        swingJoint = GetComponent<ConfigurableJoint>();
    }

    void Update()
    {
        // if(swingJoint.connectedBody) {
        if (swingJoint != null)
        {
            swingLineRenderer.SetPosition(0, (swingJoint.connectedBody.position) + swingJoint.connectedAnchor);
            swingLineRenderer.SetPosition(1, transform.position);
        }
        else if (isWebStillAttached)
        {
            swingLineRenderer.SetPosition(0, Vector3.zero);
            swingLineRenderer.SetPosition(1, Vector3.zero);

            isWebStillAttached = false;
            StartCoroutine(DestroyWebInNSeconds(1.0f));
            // Destroy(gameObject);
        }
        // }
        // else {
        //     swingLineRenderer.SetPosition(0, backupPosition);
        //     swingLineRenderer.SetPosition(1, transform.TransformPoint(swingJoint.anchor));
        // }
    }

    private IEnumerator DestroyWebInNSeconds(float secondsUntilDestroy) {
        yield return new WaitForSeconds(secondsUntilDestroy);
        Destroy(gameObject);
        yield return 0;
    }
}
