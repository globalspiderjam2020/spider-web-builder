﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSmoothRig : MonoBehaviour
{
    public Transform camera;
    public Transform focus;
    public Transform player;

    public float focusMoveLerp = 10f;

    private Vector3 midPoint;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        midPoint = player.transform.position / 6f;

        focus.transform.position = Vector3.Lerp(focus.transform.position, midPoint, Time.deltaTime * focusMoveLerp);

        camera.LookAt(focus);
    }
}
