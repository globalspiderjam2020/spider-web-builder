﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_CompositeCamera : MonoBehaviour
{
    public Material mat_comp;
    public Material mat_chromatic;
    public RenderTexture rt_comp;
    private Camera cam;
    private void Awake()
    {
        cam = GetComponent<Camera>();
    }
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        RenderTexture temp = source;
        Graphics.Blit(source, temp, mat_comp);
        Graphics.Blit(temp, destination, mat_chromatic);
        Graphics.DrawTexture(new Rect (0, 0, Screen.width, Screen.height), rt_comp);
    }
}
