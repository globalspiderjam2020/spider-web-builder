﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PostProcessing1 : MonoBehaviour
{
    public Material mat_pixelate;
    public Material mat_chromatic;

    [ExecuteInEditMode]
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        RenderTexture temp = source;
        Graphics.Blit(source, destination, mat_pixelate);
        //Graphics.Blit(temp, destination, mat_chromatic);
    }
}
