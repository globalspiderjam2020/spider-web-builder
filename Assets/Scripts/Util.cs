﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Util : MonoBehaviour
{
    public delegate void Command();

    public class Timer
    {
        public Command command;
        public float target_time;
        public float current_time;

        public Timer(Command command, float target_time)
        {
            this.command = command;
            this.target_time = target_time;
            this.current_time = 0;
        }

        public void Tick(float time)
        {
            current_time += time;
            if (current_time >= target_time)
            {
                command();
            }
        }

        public void Reset()
        {
            this.current_time = 0;
        }
    }
}
