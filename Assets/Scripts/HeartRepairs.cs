﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartRepairs : MonoBehaviour
{
    public int repairCount = 0;

    public enum HeartState { Broken, SlightlyRepaired, MostlyRepaired, FullyRepaired };
    public HeartState state = HeartState.Broken;

    void Start()
    {
        if (GameManager.Instance != null)
        {
            if (GameManager.Instance.heartRepairsScript == null)
            {
                GameManager.Instance.heartRepairsScript = this;
            }
        }
    }

    void Update()
    {
        // check and see if heart has enough repairs to move to next state
        switch (state)
        {
            case HeartState.Broken:
                break;
            case HeartState.SlightlyRepaired:
                break;
            case HeartState.MostlyRepaired:
                break;
            case HeartState.FullyRepaired:
                break;
        }

    }

    public void CheckHeartRepair(Rigidbody rb3d_1, Rigidbody rb3d_2)
    {
        bool isValid = IsValidHeartRepair(rb3d_1, rb3d_2);

        if (isValid)
        {
            repairCount++;
        }

        CheckStateChange();
    }

    private bool IsValidHeartRepair(Rigidbody rb3d_1, Rigidbody rb3d_2)
    {
        if (rb3d_1.transform.tag == "leftHalfHeart")
        {
            if (rb3d_2.transform.tag == "rightHalfHeart")
            {
                return true;
            }
        }
        else if (rb3d_1.transform.tag == "rightHalfHeart")
        {
            if (rb3d_2.transform.tag == "leftHalfHeart")
            {
                return true;
            }
        }

        return false;
    }

    private void CheckStateChange()
    {
        switch (state)
        {
            case HeartState.Broken:
                if (repairCount == 3)
                {
                    state = HeartState.SlightlyRepaired;
                }
                break;
            case HeartState.SlightlyRepaired:
                if (repairCount == 6)
                {
                    state = HeartState.MostlyRepaired;
                }
                break;
            case HeartState.MostlyRepaired:
                if (repairCount >= 10)
                {
                    state = HeartState.FullyRepaired;
                }
                break;
            case HeartState.FullyRepaired:
                break;
        }
    }
}
