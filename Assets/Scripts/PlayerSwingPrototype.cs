﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSwingPrototype : MonoBehaviour
{
    [Header("Start Parameters")]
    public float startSwingLength = 10f;

    [Header("Move Parameters")]
    public float moveSidewaysForce;
    public float moveRetractSpeed;
    public float moveExtendSpeed;
    private float maxStringLength = 20f;

    [Header("Attach Parameters")]
    public float attachDistance = 2f;
    public float worldWebSlack = 1.0f;
    public LayerMask attachLayerMask;

    [Header("References")]
    public GameObject swingAnchor;
    public GameObject replacementWeb;
    public GameObject webFixedJoint;
    public ConfigurableJoint[] swingJoints;
    public GameObject anchorParticles;

    [Header("Sound References")]
    public GenericSoundPlayer stringsound;

    private Rigidbody myRB;
    private ConfigurableJoint swingJoint;
    private LineRenderer swingLineRenderer;

    private float currentSwingLength;

    private Collider[] hitColliders;
    private Collider colliderToUse;

    public enum SpiderMoveState { none, left, right };
    public SpiderMoveState moveStateForWind;



    void Start()
    {
        myRB = GetComponent<Rigidbody>();
        swingLineRenderer = GetComponent<LineRenderer>();
        swingJoint = GetComponent<ConfigurableJoint>();

        //Set Swing Joint to Start length
        currentSwingLength = startSwingLength;
        SetSwingJointLimit(currentSwingLength);
        moveStateForWind = SpiderMoveState.none;
    }
    
    void FixedUpdate()
    {
        if (GameManager.Instance.isTakingPlayerInput)
        {
            if (Input.GetKey("a")) //move left
            {
                myRB.AddForce(Vector3.right * -1 * moveSidewaysForce);
                moveStateForWind = SpiderMoveState.left;
            }
            else if (Input.GetKey("d")) //move right
            {
                myRB.AddForce(Vector3.right * moveSidewaysForce);
                moveStateForWind = SpiderMoveState.right;
            }
            else if (!Input.GetKey("a") && !Input.GetKey("d"))
            {
                moveStateForWind = SpiderMoveState.none;
            }

            if (Input.GetMouseButton(0)) //retract rope
            {
                currentSwingLength -= moveRetractSpeed / 100f;
                SetSwingJointLimit(currentSwingLength);
            }
            else if (Input.GetMouseButton(1) && currentSwingLength < maxStringLength)  //extend rope
            {
                currentSwingLength += moveExtendSpeed / 100f;
                SetSwingJointLimit(currentSwingLength);
            }

            GameManager.Instance.spidersMovementState = moveStateForWind;
        }
    }
    private void Update()
    {
        if (GameManager.Instance.isTakingPlayerInput)
        {
            if (Input.GetKeyUp("space"))
            {
                NewAnchor();
            }
        }
    }

    private void NewAnchor()
    {
        hitColliders = Physics.OverlapSphere(transform.position, attachDistance, attachLayerMask);

        if (hitColliders.Length > 0)
        {
            colliderToUse = hitColliders[0];
            //Vector3 dir = transform.position - hitColliders[0].Clos
            if (hitColliders.Length > 1)
            {
                for (int i = 0; i < hitColliders.Length; i++)
                {
                    if (hitColliders[i].transform.tag == "leftHalfHeart" || hitColliders[i].transform.tag == "rightHalfHeart") 
                    {
                        colliderToUse = hitColliders[i];
                        break;
                    }
                }
            }

            ConfigurableJoint thisJoint = colliderToUse.gameObject.GetComponent<ConfigurableJoint>();

            if ((colliderToUse.gameObject.layer == 8) || ((thisJoint != null) && (thisJoint.connectedBody != null)))
            {
                stringsound.EmitSound();

                transform.position = colliderToUse.ClosestPoint(transform.position);

                Instantiate(anchorParticles, transform.position, Quaternion.identity);
                GameManager.Instance.cameraShakeScript.AnchorCamShake();

                CreateReplacementWeb();

                swingAnchor.transform.position = transform.position;

                currentSwingLength = startSwingLength;
                SetSwingJointLimit(currentSwingLength);
            }
        }
    }

    private void SetSwingJointLimit(float limitValue)
    {

        SoftJointLimit softJointLimit = new SoftJointLimit();
        softJointLimit.limit = limitValue;
        swingJoint.linearLimit = softJointLimit;
    }

    private void CreateReplacementWeb()
    {
        GameObject newWebHolder = new GameObject("New Web Group");
        newWebHolder.transform.position = swingAnchor.transform.position;

        //---------------------FIND ENVIRONMENT RIGIDBODIES
        Collider[] hitColliders1 = Physics.OverlapSphere(swingAnchor.transform.position, attachDistance, attachLayerMask);
        Collider colliderToUse1 = hitColliders1[0];
        if (hitColliders1.Length > 1)
        {
            for (int i = 0; i < hitColliders1.Length; i++)
            {
                if (hitColliders1[i].transform.tag == "leftHalfHeart" || hitColliders1[i].transform.tag == "rightHalfHeart")
                {
                    colliderToUse1 = hitColliders1[i];
                    break;
                }
            }
        }
        Rigidbody attach1RB = colliderToUse1.GetComponent<Rigidbody>();

        //Collider[] hitColliders2 = Physics.OverlapSphere(transform.position, attachDistance, attachLayerMask);
        Rigidbody attach2RB = colliderToUse.GetComponent<Rigidbody>();


        //---------------------DISTANCES
        Vector3 dist1 = (swingAnchor.transform.position - transform.position) * (1.0f/3.0f);
        Vector3 dist2 = (swingAnchor.transform.position - transform.position) * (2.0f/3.0f);
        Vector3 dist3 = (swingAnchor.transform.position - transform.position);


        //---------------------WEB 1
        GameObject newWeb1 = Instantiate(replacementWeb, swingAnchor.transform.position - dist1, Quaternion.identity);
        newWeb1.transform.LookAt(swingAnchor.transform.position);

        
        GameObject newWebFixed1 = Instantiate(webFixedJoint, swingAnchor.transform.position, Quaternion.identity);
        newWebFixed1.GetComponent<FixedJoint>().connectedBody = attach1RB;
        ConfigurableJoint newJoint1 = newWeb1.GetComponent<ConfigurableJoint>();
        newJoint1.connectedBody = newWebFixed1.GetComponent<Rigidbody>();
        newJoint1.anchor = new Vector3(newJoint1.anchor.x, newJoint1.anchor.y,  dist1.magnitude + worldWebSlack);

        // SoftJointLimit softJointLimit1 = new SoftJointLimit();
        // softJointLimit1.limit = dist1.magnitude + worldWebSlack;
        // // softJointLimit1.limit = 7f;
        // newJoint1.linearLimit = softJointLimit1;

        //---------------------WEB 2
        GameObject newWeb2 = Instantiate(replacementWeb, swingAnchor.transform.position - dist2, Quaternion.identity);
        newWeb2.transform.LookAt(swingAnchor.transform.position);
        ConfigurableJoint newJoint2 = newWeb2.GetComponent<ConfigurableJoint>();
        newJoint2.connectedBody = newWeb1.GetComponent<Rigidbody>();
        // SoftJointLimit softJointLimit2 = new SoftJointLimit();
        // softJointLimit2.limit = dist1.magnitude + worldWebSlack;
        // // softJointLimit2.limit = 6f;
        // newJoint2.linearLimit = softJointLimit2;
        newJoint2.anchor = new Vector3(newJoint2.anchor.x, newJoint2.anchor.y,  dist1.magnitude + worldWebSlack);


        //---------------------WEB 3
        GameObject newWeb3 = Instantiate(replacementWeb, transform.position, Quaternion.identity);
        newWeb3.transform.LookAt(swingAnchor.transform.position);


        GameObject newWebFixed2 = Instantiate(webFixedJoint, transform.position, Quaternion.identity);
        newWebFixed2.GetComponent<FixedJoint>().connectedBody = attach2RB;


        ConfigurableJoint newJoint3 = newWeb3.GetComponent<ConfigurableJoint>();
        ConfigurableJoint newJointFixed1 = newWebFixed2.AddComponent<ConfigurableJoint>() as ConfigurableJoint;
        newJointFixed1.anchor = Vector3.zero;

        newJointFixed1.xMotion = ConfigurableJointMotion.Limited;
        newJointFixed1.yMotion = ConfigurableJointMotion.Limited;
        newJointFixed1.zMotion = ConfigurableJointMotion.Limited;
        

        SoftJointLimit softJointLimitFixed1 = new SoftJointLimit();
        softJointLimitFixed1.limit = worldWebSlack;
        newJointFixed1.linearLimit = softJointLimitFixed1;
        
        newJointFixed1.connectedBody = newWeb3.GetComponent<Rigidbody>();
        
        newJoint3.connectedBody = newWeb2.GetComponent<Rigidbody>();
        // SoftJointLimit softJointLimit3 = new SoftJointLimit();
        // softJointLimit3.limit = dist1.magnitude + worldWebSlack;
        // newJoint3.linearLimit = softJointLimit3;
        newJoint3.anchor = new Vector3(newJoint3.anchor.x, newJoint3.anchor.y,  dist1.magnitude + worldWebSlack);

        newWebFixed1.transform.parent = newWebHolder.transform;
        newWeb1.transform.parent = newWebHolder.transform;
        newWeb2.transform.parent = newWebHolder.transform;
        newWeb3.transform.parent = newWebHolder.transform;
        newWebFixed2.transform.parent = newWebHolder.transform;

        if (GameManager.Instance.state != GameManager.GameState.HeartRepair)
        {
            newWebHolder.transform.parent = DontDestroyBlockout.Instance.mainSceneWebs.transform;
        }

        GameManager.Instance.IncrementWebCount();
        //Debug.LogError("Meow");
        HeartRepairs hr = attach1RB.GetComponentInParent<HeartRepairs>();
        if (hr != null)
        {
            hr.CheckHeartRepair(attach1RB, attach2RB);
        }
    }
}
