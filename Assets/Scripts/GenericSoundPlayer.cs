﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericSoundPlayer : MonoBehaviour
{
    private AudioSource audio;

    public bool playOnAwake = false;

    public bool cutOffPreviousSound = false;

    public AudioClip[] sounds;

    [Range(0.0f, 0.5f)]
    public float pitchRandomness = 0.08f;

    private int lastRand = -1;
    private int rand = 0;

    private float startingPitch;


    void Awake()
    {
        audio = GetComponent<AudioSource>();
        startingPitch = audio.pitch;

        if (playOnAwake)
        {
            EmitSound();
        }
    }

    public void EmitSound()
    {
        if (!audio.isPlaying || cutOffPreviousSound)
        {
            if (sounds.Length > 1)
            {
                do
                {
                    rand = Random.Range(0, sounds.Length);
                } while (lastRand == rand);
            }

            audio.clip = sounds[rand];
            audio.pitch = startingPitch + Random.Range(-pitchRandomness, pitchRandomness);
            audio.Play();
            lastRand = rand;
        }
    }
    public void StopSound()
    {
        audio.Stop();
    }
}
