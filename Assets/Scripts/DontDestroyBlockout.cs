﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyBlockout : MonoBehaviour
{
    public static DontDestroyBlockout Instance;

    public GameObject mainSceneWebs;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else if (Instance != this)
        {
            Destroy(this.gameObject);
        }
    }
}
