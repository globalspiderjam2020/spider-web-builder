﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    [Header("Fly")]
    public float flyEatDuration;
    public float flyEatMagnitude;

    [Header("WebBreak")]
    public float webBreakDuration;
    public float webBreakMagnitude;

    private void Start()
    {
        if (GameManager.Instance != null)
        {
            //if (GameManager.Instance.cameraShakeScript == null)
            //{
                GameManager.Instance.cameraShakeScript = this;
            //}
        }
    }

    public void FlyEatCamShake()
    {
        StartCoroutine(CamShake(flyEatDuration, flyEatMagnitude));
    }

    public void AnchorCamShake()
    {
        StartCoroutine(CamShake(flyEatDuration, flyEatMagnitude/2));
    }

    public void WebBreakCamShake()
    {
        StartCoroutine(CamShake(webBreakDuration, webBreakMagnitude));
    }

    public IEnumerator CamShake(float duration, float magnitude)
    {
        Vector3 orignalPosition = transform.position;
        float _timer = duration;

        while (_timer > 0.0f)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            transform.position = new Vector3(x, y, transform.position.z);
            _timer -= Time.deltaTime;
            yield return 0;
        }
        transform.position = orignalPosition;
    }
}
