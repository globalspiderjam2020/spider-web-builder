﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_PostProcessDepth : MonoBehaviour
{
    public Material mat_pixelate;
    public Material mat_depth;

    private void Awake()
    {
        GetComponent<Camera>().depthTextureMode = DepthTextureMode.Depth;
    }

    [ExecuteInEditMode]
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        RenderTexture temp = source;
        //Graphics.Blit(source, temp, mat_pixelate);
        Graphics.Blit(source, destination, mat_depth);
    }
}
