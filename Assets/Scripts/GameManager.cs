﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public enum GameState { TitleCard, WebBuild, HeartRepair, WebRepair };

    [Header("Game State")]
    public GameState state = GameState.TitleCard;
    public bool isTakingPlayerInput = false;
    public bool isFirstTimePlaying = true;
    public PlayerSwingPrototype.SpiderMoveState spidersMovementState;
    public PlayerSwingPrototype.SpiderMoveState oldMoveState;
    private bool isWindSlowedDown = false;

    [Header("Timers (Seconds)")]
    public float secondsUntilGameStarts = 2.0f;
    public float secondsUntilHeartBreak = 30.0f;
    public float secondsUntilHeartLevelLoads = 2.0f;
    public float secondsUntilWebRepairLevelLoads = 2.0f;
    public float secondsUntilWebRepairPlayerControlStarts = 2.0f;
    public float secondsInGame = 0.0f;

    [SerializeField]
    private float _timer;

    [Header("Score Coutners")]
    public int flysCaughtCount = 0;
    public int websMadeCount = 0;
    public int heartRepairCount = 0;

    [Header("GameObject/Script References")]
    public HeartRepairs heartRepairsScript;
    public CameraShake cameraShakeScript;
    public FlipWindParticles windParticlesScript;

    public PlayableDirector dir_title_card;
    public PlayableDirector dir_heart_break;
    public Scr_ZoomCamera scr_zoom;
    public AudioSource aud_music;
    public GameObject obj_hand;
    public GameObject obj_tml_heart;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else if (Instance != this)
        {
            Destroy(this.gameObject);
        }

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked; 
    }

    void Start()
    {
        _timer = secondsUntilGameStarts;
        secondsInGame = 0.0f;
        spidersMovementState = PlayerSwingPrototype.SpiderMoveState.none;
        oldMoveState = spidersMovementState;
    }
    
    void Update()
    {
        secondsInGame += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        switch (state)
        {
            case GameState.TitleCard:
                GameStateTitleCard();
                break;
            case GameState.WebBuild:
                GameStateWebBuild();
                break;
            case GameState.HeartRepair:
                GameStateHeartRepair();
                break;
            case GameState.WebRepair:
                GameStateWebRepair();
                break;
        }

        if (oldMoveState != spidersMovementState)
        {
            oldMoveState = spidersMovementState;
            if (state != GameState.HeartRepair)
            {
                if (windParticlesScript != null)
                {
                    if (spidersMovementState == PlayerSwingPrototype.SpiderMoveState.none)
                    {
                        windParticlesScript.StopWindSpeed();
                    }
                    else if (spidersMovementState == PlayerSwingPrototype.SpiderMoveState.right)
                    {
                        windParticlesScript.ChangeZSpeed(true);
                    }
                    else if (spidersMovementState == PlayerSwingPrototype.SpiderMoveState.left)
                    {
                        windParticlesScript.ChangeZSpeed(false);
                    }
                }
            }
        }
    }





    public void IncrementFlyCount()
    {
        flysCaughtCount++;
    }

    public void IncrementWebCount()
    {
        websMadeCount++;
    }





    private void GameStateTitleCard()
    {
        _timer -= Time.deltaTime;

        if (windParticlesScript != null)
        {
            if ((secondsInGame > 4.0f) && !isWindSlowedDown)
            {
                isWindSlowedDown = true;
                windParticlesScript.StopWindSpeed();
            }
        }

        if (_timer <= 0.0f)
        {
            _timer = secondsUntilHeartBreak;
            state = GameState.WebBuild;
            isTakingPlayerInput = true;
        }
    }

    private void GameStateWebBuild()
    {
        _timer -= Time.deltaTime;

        if (_timer <= 0.0f)
        {
            state = GameState.HeartRepair;
            isTakingPlayerInput = false;
            //dir_heart_break.Play();
            aud_music.Stop();
            obj_hand.SetActive(true);
            secondsUntilHeartBreak += 30;
            //scr_zoom.is_zooming = true;
            StartCoroutine(WaitNSecondsForSceneLoad(secondsUntilHeartLevelLoads, 1, false));
            StartCoroutine(PlayTransition(7));
        }
    }
    IEnumerator PlayTransition(float secondsToWait)
    {
        yield return new WaitForSeconds(secondsToWait);
        dir_heart_break.Play();
        scr_zoom.is_zooming = true;
        yield return null;
    }

    private void GameStateHeartRepair()
    {
        if (heartRepairsScript != null)
        {
            if (!isTakingPlayerInput)
            {
                isTakingPlayerInput = true;
            }

            heartRepairCount = heartRepairsScript.repairCount;
            if (heartRepairsScript.state == HeartRepairs.HeartState.FullyRepaired)
            {
                _timer = secondsUntilWebRepairPlayerControlStarts;
                state = GameState.WebRepair;
                isFirstTimePlaying = false;
                obj_tml_heart.GetComponent<PlayableDirector>().Play();
                isTakingPlayerInput = false;
                StartCoroutine(WaitNSecondsForSceneLoad(secondsUntilWebRepairLevelLoads, 0, true));
            }
        }
    }

    private void GameStateWebRepair()
    {
        // dont need to do this if we stay in the same scene
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            _timer -= Time.deltaTime;

            if (_timer <= 0.0f)
            {
                if (!isTakingPlayerInput)
                {
                    isTakingPlayerInput = true;
                    _timer = secondsUntilHeartBreak;
                    state = GameState.WebBuild;
                }
            }
        }
    }





    //IEnumerator WaitNSeconds(float secondsToWait)
    //{
    //    yield return new WaitForSeconds(secondsToWait);
    //}
    
    IEnumerator WaitNSecondsForSceneLoad(float secondsToWait, int sceneToLoad, bool isBlockActive)
    {
        yield return new WaitForSeconds(secondsToWait);
        if (DontDestroyBlockout.Instance != null)
        {
            DontDestroyBlockout.Instance.gameObject.SetActive(isBlockActive);
        }
        SceneManager.LoadScene(sceneToLoad);

        yield return null;
    }
}
