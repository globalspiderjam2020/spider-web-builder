﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlySpawner : MonoBehaviour
{

    public float spawnRate = 5f;
    public Vector2 spawnRange;
    public GameObject flyPrefab;

    private float spawnTimer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        spawnTimer += Time.deltaTime;

        if (spawnTimer > spawnRate)
        {
            SpawnFly();
            spawnTimer = 0;
        }
    }

    void SpawnFly()
    {
        Vector3 spawnPoint = new Vector3(Random.Range(-spawnRange.x, spawnRange.x), Random.Range(-spawnRange.y, spawnRange.y), transform.position.z);
        Instantiate(flyPrefab, spawnPoint, Quaternion.identity, transform);
    }
}
