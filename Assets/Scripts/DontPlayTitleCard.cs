﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontPlayTitleCard : MonoBehaviour
{
    public GameObject Tml_HeartBreak;
    public GameObject Tml_TitleCard;

    public GameObject Text_Title;
    public GameObject Text_Credits;
    public GameObject Text_Controls_AD;
    public GameObject Text_Controls_LeftRightMouse;
    public GameObject Text_Controls_Web;
    public GameObject Text_Controls_Quit;


    private bool isFirstTimePlaying = true;

    void Start()
    {
        if (GameManager.Instance != null)
        {
            isFirstTimePlaying = GameManager.Instance.isFirstTimePlaying;
        }

        if (!isFirstTimePlaying)
        {
            //Tml_HeartBreak.SetActive(false);
            Tml_TitleCard.SetActive(false);
            Text_Title.SetActive(false);
            Text_Credits.SetActive(false);
            Text_Controls_AD.SetActive(false);
            Text_Controls_LeftRightMouse.SetActive(false);
            Text_Controls_Web.SetActive(false);
            Text_Controls_Quit.SetActive(false);
        }
    }
}
