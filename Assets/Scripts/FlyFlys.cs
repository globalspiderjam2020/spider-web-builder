﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyFlys : MonoBehaviour
{
    public float flySpeed = 2f;

    private bool splat = false;

    public GameObject splatParticles;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!splat)
        {
            transform.position += Vector3.forward * flySpeed;
        }

        if (transform.position.z > 30f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!splat)
        {
            GetComponent<GenericSoundPlayer>().EmitSound();
            Instantiate(splatParticles, transform.position, Quaternion.identity);

            //Debug.LogError("meowMix");
            GetComponent<Rigidbody>().isKinematic = false;

            FixedJoint myFix = gameObject.AddComponent(typeof(FixedJoint)) as FixedJoint;
            myFix.connectedBody = collision.gameObject.GetComponent<Rigidbody>();

            splat = true;
        }
    }
}
