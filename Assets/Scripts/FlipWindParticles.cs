﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipWindParticles : MonoBehaviour
{
    private ParticleSystem ps;
    private ParticleSystem.VelocityOverLifetimeModule velOverLifetime;
    private ParticleSystem.EmissionModule emiss;
    public float psLinearSpeedZ = 12.0f;
    public Coroutine windCoroutine;

    void Start()
    {
        ps = GetComponent<ParticleSystem>();
        velOverLifetime = ps.velocityOverLifetime;
        emiss = ps.emission;
        if (GameManager.Instance != null)
        {
            GameManager.Instance.windParticlesScript = this;
        }
    }

    public void ChangeZSpeed(bool isDirectionRight)
    {
        if (windCoroutine != null)
        {
            StopCoroutine(windCoroutine);
        }
        windCoroutine = StartCoroutine(FlipZSpeed(isDirectionRight));
    }

    public void StopWindSpeed()
    {
        if (windCoroutine != null)
        {
            StopCoroutine(windCoroutine);
        }
        windCoroutine = StartCoroutine(StopWind());
    }

    IEnumerator FlipZSpeed(bool isDirectionRight)
    {
        velOverLifetime.y = 0.5f;
        emiss.rateOverTime = 6.0f;

        if (isDirectionRight)
        {
            float oldSpeed = velOverLifetime.z.constant;
            float newSpeed = psLinearSpeedZ;
            float currentSpeed = oldSpeed;

            while (currentSpeed < newSpeed)
            {
                currentSpeed += Time.deltaTime * 20.0f;
                velOverLifetime.z = currentSpeed;

                yield return 0;
            }
        }
        else
        {
            float oldSpeed = velOverLifetime.z.constant;
            float newSpeed = psLinearSpeedZ * -1;
            float currentSpeed = oldSpeed;

            while (currentSpeed > newSpeed)
            {
                currentSpeed -= Time.deltaTime * 20.0f;
                velOverLifetime.z = currentSpeed;

                yield return 0;
            }
        }

        yield return 0;
    }

    IEnumerator StopWind()
    {
        velOverLifetime.y = -0.5f;
        emiss.rateOverTime = 0.5f;

        float oldSpeed = velOverLifetime.z.constant;
        float newSpeed = 0.1f;
        float currentSpeed = oldSpeed;

        bool isRight = false;

        if (oldSpeed > newSpeed)
        {
            isRight = true;
        }

        if (isRight)
        {
            while (currentSpeed > newSpeed)
            {
                currentSpeed -= Time.deltaTime * 20.0f;
                velOverLifetime.z = currentSpeed;

                yield return 0;
            }
        }
        else
        {
            while (currentSpeed < newSpeed)
            {
                currentSpeed += Time.deltaTime * 20.0f;
                velOverLifetime.z = currentSpeed;

                yield return 0;
            }
        }

        yield return 0;
    }
}
