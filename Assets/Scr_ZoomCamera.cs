﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_ZoomCamera : MonoBehaviour
{
    public bool is_zooming;
    public GameObject obj_spider;
    public float accel;
    public float max_speed;

    private float current_speed;

    private Rigidbody rb;

    private void Start()
    {
        GameManager.Instance.scr_zoom = this;
        rb = GetComponent<Rigidbody>();
        current_speed = 1;
    }

    private void Update()
    {
        if (is_zooming)
        {
            if (current_speed < max_speed)
            {
                current_speed += accel;
            }

            transform.LookAt(obj_spider.transform);
            rb.velocity = transform.forward * current_speed;
        }
    }
}
