﻿Shader "Unlit/Shd_Brownian"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float random (in float2 _st) {
                return frac(sin(dot(_st.xy,
                                    float2(12.9898,78.233)))*
                    43758.5453123);
            }

            float noise (in float2 _st) {
                float2 i = floor(_st);
                float2 f = frac(_st);

                // Four corners in 2D of a tile
                float a = random(i);
                float b = random(i + float2(1.0, 0.0));
                float c = random(i + float2(0.0, 1.0));
                float d = random(i + float2(1.0, 1.0));

                float2 u = f * f * (3.0 - 2.0 * f);

                return lerp(a, b, u.x) +
                        (c - a)* u.y * (1.0 - u.x) +
                        (d - b) * u.x * u.y;
            }

            float fbm ( in float2 _st) {
                float v = 0.0;
                float a = 0.5;
                float2 shift = float2(100.0, 100.0);
                // Rotate to reduce axial bias
                float2x2 rot = float2x2(cos(0.5), sin(0.5),
                                -sin(0.5), cos(0.50));
                for (int i = 0; i < 8; ++i) {
                    v += a * noise(_st);
                     _st = mul(rot, _st) * 2.0 + shift;
                    a *= 0.5;
                }
                return v;
            }

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 st = i.uv;//gl_FragCoord.xy/u_resolution.xy*3.;
                // st += st * abs(sin(u_time*0.1)*3.0);
                float3 color = float3(0.985,0.976,0.922);

                float2 q = float2(0, 0);
                q.x = fbm( st + 0.00*_Time.y);
                q.y = fbm( st + float2(1.0, 1.0));

                float2 r = float2(0, 0);
                r.x = fbm( st + 1.0*q + float2(1.7,9.2)+ 0.15*_Time.y);
                r.y = fbm( st + 1.0*q + float2(8.3,2.8)+ 0.126*_Time.y);

                float f = fbm(st+r);

                return float4((f*f*f+.6*f*f+.5*f)*color,1.);
            }
            ENDCG
        }
    }
}
